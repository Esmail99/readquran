import axios from 'axios';
import {getStorage} from '../services/localStorageManager';

const postService = async (route, data) => {
  const user = await getStorage('data');
  const userToken = user.token;
  const headers = {
    'Content-Type': 'application/json',
    tokenHeader: `${userToken}`,
  };
  const res = await axios.post(
    `https://7astc125f5.execute-api.us-east-1.amazonaws.com/Dev_template/${route}`,
    data,
    {headers: headers},
  );
  return res;
};

export default postService;
