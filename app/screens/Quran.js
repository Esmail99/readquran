import React from 'react';
import {StyleSheet, FlatList, Text, View} from 'react-native';
import {Title, Button} from 'native-base';
import connect from '../connectedComponent/index';
import QuranList from '../components/quranView/quranList';
import realm, {queryAllItems} from '../database/allSchemas';
import Loading from '../components/shared/Loading';
import Layout from '../components/shared/Layout';
import {Actions} from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/AntDesign';

class Quran extends React.Component {
  constructor() {
    super();
    this.state = {
      Suar: [],
    };
    this.reloadData();
    realm.addListener('change', () => {
      this.reloadData();
    });
  }

  reloadData = () => {
    queryAllItems()
      .then(Suar => {
        this.setState({Suar});
      })
      .catch(error => {
        this.setState({Suar: []});
      });
  };

  renderItem = ({item}) => {
    return <QuranList sora={item} />;
  };

  render() {
    const {suar, suarLoading} = this.props;
    const translate = this.props.language.text;

    return suarLoading && suar.length === 0 ? (
      <Loading />
    ) : (
      <Layout>
        <Title allowFontScaling={false} style={styles.title}>
          {translate.Reading}
        </Title>
        <View style={styles.settings}>
          <Icon
            name={'setting'}
            size={25}
            color={'#8b8b8b'}
            onPress={() => Actions.editAccount()}
          />
        </View>
        <FlatList
          data={this.state.Suar}
          renderItem={this.renderItem}
          keyExtractor={(item, index) => index.toString()}
        />
        <Button
          style={styles.shareButton}
          onPress={() => Actions.wrongReading()}>
          <Text style={styles.text}>Go to wrong ayat</Text>
        </Button>
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    color: '#444444',
    alignSelf: 'center',
    fontSize: 28,
    fontFamily: 'ElMessiriBold',
    textTransform: 'capitalize',
    lineHeight: 44,
    paddingVertical: 5,
    borderBottomWidth: 1,
    borderBottomColor: '#DBDBDB',
    width: '100%',
    marginTop: '2%',
  },
  shareButton: {
    position: 'absolute',
    right: 20,
    bottom: 20,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    alignSelf: 'center',
    borderRadius: 15,
    paddingHorizontal: 8,
    borderWidth: 1,
    borderColor: 'red',
  },
  text: {
    fontSize: 15,
    color: '#707070',
  },
  settings: {
    position: 'absolute',
    right: 15,
    top: 15,
  },
});

export default connect(Quran);
