import React, {Component} from 'react';
import {StyleSheet, View, Text, Image, TouchableOpacity} from 'react-native';
import {Content, Segment, Button} from 'native-base';
import Header from '../components/shared/MinHeader';
import connect from '../connectedComponent';
import Layout from '../components/shared/Layout';
import ImamButton from '../components/shared/imamButton';
import ImamInput from '../components/shared/imamInput';
import {Actions} from 'react-native-router-flux';
import postService from '../axios/postService';
import {setStorage, getStorage} from '../services/localStorageManager';
import Toaster, {ToastStyles} from 'react-native-toaster';
import ImagePicker from 'react-native-image-picker';
import DeviceInfo from 'react-native-device-info';

class EditAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      updatedImage: null,
      username: this.props?.userData?.user?.username,
      usernameError: '',
      email: this.props?.userData?.user?.email,
      emailError: '',
      gender: this.props?.userData?.user?.gender,
      loading: false,
      disabled: false,
      message: null,
      androidId: '',
    };
  }

  componentDidMount = () => {
    // console.log('mmm: ', this.props.imageUrl);
    if (this.props.userImage) {
      this.setState({userImage: this.props.userImage});
    }
    DeviceInfo.getAndroidId().then(androidId => {
      this.setState({androidId});
    });
    this.props.fetchProfileImage();
  };

  onEventPress = event => {
    this.setState({gender: event, disabled: true});
  };

  getName = data => {
    this.setState({
      username: data.value,
      usernameError: data.error,
      disabled: true,
    });
  };

  getEmail = data => {
    this.setState({
      email: data.value.replace(' ', ''),
      emailError: data.error,
      disabled: true,
    });
  };

  updateUserInfo = async () => {
    const {username, email, gender, updatedImage} = this.state;

    if (!email || !username || !gender) {
      return;
    }
    this.setState({loading: true});

    const user_id = this.props?.userData?.user?.id;
    const requestData = updatedImage
      ? {
          user_id,
          gender,
          username,
          email,
          image: updatedImage,
        }
      : {
          user_id,
          gender,
          username,
          email,
        };

    postService('user/update-user', requestData)
      .then(async res => {
        console.log('resO: ', res.data);
        let message = '';
        if (res.data?.errorMessage) {
          if (res.data.errorMessage.includes('email')) {
            message = {
              text: 'Email already exist!',
              styles: ToastStyles.error,
            };
          } else if (res.data?.errorMessage?.includes('username')) {
            message = {
              text: 'Username already exist!',
              styles: ToastStyles.error,
            };
          } else {
            message = {
              text: res.data.errorMessage,
              styles: ToastStyles.error,
            };
          }
          this.setState({loading: false, message});
        } else {
          const data = {
            token: {...(await getStorage('data'))}.token,
            user: res.data,
          };
          await setStorage({key: 'data', data});
          await this.props.updateUserData();
          this.props.fetchProfileImage();
          this.setState({loading: false});
          Actions.pop();
        }
      })
      .catch(err => {
        console.log(err);
        if (err.message === 'Network Error') {
          Actions.noInternet();
        } else {
          Actions.noInternet({error: true});
        }
        this.setState({loading: false});
      });
  };

  onEditImagePress = () => {
    const options = {
      title: 'Select Avatar',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    // showImagePicker
    ImagePicker.launchImageLibrary(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        this.setState({updatedImage: response.data, disabled: true});
      }
    });
  };

  render() {
    const translate = this.props?.language?.text;
    const userImage = this.props.imageUrl;
    const {
      loading,
      disabled,
      updatedImage,
      gender,
      username,
      email,
      usernameError,
      emailError,
      message,
      androidId,
    } = this.state;

    return (
      <Layout>
        <Header title={translate.Edit_Account} showTitle={true} />
        <Content style={styles.content}>
          <View style={styles.userInfo}>
            <Image
              source={
                updatedImage
                  ? {uri: 'data:image/jpeg;base64,' + updatedImage}
                  : userImage
              }
              style={styles.userImg}
            />
            <TouchableOpacity
              onPress={this.onEditImagePress}
              style={styles.editImageContainer}>
              <Image
                source={require('../assets/img/editImage.png')}
                style={styles.editImage}
              />
            </TouchableOpacity>
          </View>
          <ImamInput
            label={translate.Username}
            placeholder={
              username === `guest${androidId}` ? 'type your username' : username
            }
            errorMesssage={translate.Username_is_required}
            getValueAndError={this.getName}
            type={'text'}
          />
          <ImamInput
            label={translate.Email}
            placeholder={
              (this.props?.userData?.user?.facebook_id &&
                this.props?.userData?.user?.email === null) ||
              email === `${androidId}@guest.com`
                ? 'mail@mail.com'
                : email
            }
            errorMesssage={translate.Invalid_mail}
            getValueAndError={this.getEmail}
            type={'email'}
          />
          <View style={styles.genderContainer}>
            <Text style={styles.gender}>{translate.Gender}</Text>
            <Segment style={styles.segment}>
              <Button
                onPress={() => this.onEventPress('female')}
                style={
                  gender === 'female'
                    ? styles.segmentButtonActive
                    : styles.segmentButton
                }>
                <Text
                  allowFontScaling={false}
                  style={
                    gender === 'female'
                      ? styles.segmentTextActive
                      : styles.segmentText
                  }>
                  {translate.Female}
                </Text>
              </Button>
              <Button
                onPress={() => this.onEventPress('male')}
                style={
                  gender === 'male'
                    ? styles.segmentButtonActive
                    : styles.segmentButton
                }>
                <Text
                  allowFontScaling={false}
                  style={
                    gender === 'male'
                      ? styles.segmentTextActive
                      : styles.segmentText
                  }>
                  {translate.Male}
                </Text>
              </Button>
            </Segment>
          </View>
          <ImamButton
            loading={loading}
            disabled={disabled && !usernameError && !emailError}
            buttonText={translate.Save}
            onPress={this.updateUserInfo}
          />
          <ImamButton
            disabled={true}
            buttonText={translate.Change_password}
            onPress={() => Actions.changePassword()}
          />
        </Content>
        {message ? <Toaster message={message} duration={500} /> : null}
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    width: '93%',
    alignSelf: 'center',
    marginTop: 40,
    marginBottom: 10,
  },
  userInfo: {
    alignSelf: 'center',
  },
  userImg: {
    alignSelf: 'center',
    width: 111,
    height: 111,
    borderRadius: 100,
    borderWidth: 1,
    borderColor: '#e1dfda',
  },
  editImageContainer: {
    width: 30,
    height: 30,
    position: 'absolute',
    right: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  editImage: {
    width: 27,
    resizeMode: 'contain',
  },
  genderContainer: {
    width: 300,
    alignSelf: 'center',
  },
  gender: {
    paddingLeft: 10,
    fontFamily: 'CairoRegular',
    fontSize: 12,
    color: '#51565F',
  },
  segment: {
    backgroundColor: '#DAC7AC96',
    marginVertical: 5,
    borderRadius: 10,
    height: 40,
    justifyContent: 'center',
    padding: 5,
  },
  segmentButton: {
    borderColor: 'transparent',
    justifyContent: 'center',
    height: 33,
    backgroundColor: 'transparent',
    borderRadius: 10,
    width: '50%',
    shadowColor: '#00000029',
    shadowOffset: {
      width: 0,
      height: 3,
    },
  },
  segmentButtonActive: {
    justifyContent: 'center',
    height: 33,
    backgroundColor: 'white',
    borderRadius: 10,
    width: '50%',
  },
  segmentText: {
    fontFamily: 'segoe-ui',
    color: '#707070',
    fontSize: 13,
    lineHeight: 17,
  },
  segmentTextActive: {
    fontWeight: 'bold',
    color: '#BFA115',
    fontSize: 13,
    lineHeight: 17,
  },
});

export default connect(EditAccount);
