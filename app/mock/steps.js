const steps = [
  {
    disableStep: require('../assets/img/step1.png'),
    pressedStep: require('../assets/img/pressedStep1.png'),
  },
  {
    disableStep: require('../assets/img/step2.png'),
    pressedStep: require('../assets/img/pressedStep2.png'),
  },
  {
    disableStep: require('../assets/img/step3.png'),
    pressedStep: require('../assets/img/pressedStep3.png'),
  },
  {
    disableStep: require('../assets/img/step4.png'),
    pressedStep: require('../assets/img/pressedStep4.png'),
  },
  {
    disableStep: require('../assets/img/step5.png'),
    pressedStep: require('../assets/img/pressedStep5.png'),
  },
];

export default steps;
