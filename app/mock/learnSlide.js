import shortSurah from './shortSurah';
import ahkam from './ahkam';
// import {getStorage} from '../services/localStorageManager';

// const getLanguage = async () => {
//   return await getStorage('language');
// };
// console.log('ff', getLanguage());

const learnSlides = [
  {
    key: 'shortSurah',
    title: 'Short Surah',
    section1Title: 'Level 1',
    section2Title: 'Level 2',
    slideData: shortSurah,
  },
  {
    key: 'tajweed',
    title: 'Tajweed',
    section1Title: 'The un-voweled mim rules',
    section2Title: 'The un-voweled lam rules',
    slideData: ahkam,
  },
];

export default learnSlides;
