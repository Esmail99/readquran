import Realm from 'realm';
export const QURAN_SCHEMA = 'QuranList';
export const AYA_SCHEMA = 'Aya';
// Define your models and their properties
export const Aya = {
  name: AYA_SCHEMA,
  properties: {
    ayah_number: 'int',
  },
};

export const QuranListSchema = {
  name: QURAN_SCHEMA,
  primaryKey: 'id',
  properties: {
    id: 'int',
    number_of_ayat: 'int',
    sora_number: 'int',
    sora_name_arabic: 'string',
    sora_name_latin: 'string',
    sora_name_english: 'string',
    created_at: 'string',
    place: 'string',
    user_ayat: 'int',
    userProgress: {type: 'list', objectType: AYA_SCHEMA},
  },
};

const databaseOptions = {
  path: 'imam.realm',
  schema: [QuranListSchema, Aya],
  schemaVersion: 0, //optional
};
export const insertNewItem = item =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then(realm => {
        realm.write(() => {
          realm.create(QURAN_SCHEMA, item);
          resolve(item);
        });
      })
      .catch(error => reject(error));
  });

export const deleteAllItems = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then(realm => {
        realm.write(() => {
          let allItems = realm.objects(QURAN_SCHEMA);
          realm.delete(allItems);
          resolve();
        });
      })
      .catch(error => reject(error));
  });

export const queryAllItems = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then(realm => {
        let allItems = realm.objects(QURAN_SCHEMA);
        resolve(allItems);
      })
      .catch(error => {
        reject(error);
      });
  });

export const updateItem = async item =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then(realm => {
        realm.write(async () => {
          let updatingItem = realm.objectForPrimaryKey(QURAN_SCHEMA, item.id);
          let check = updatingItem.userProgress.filtered(
            `ayah_number = ${item.ayah_number}`,
          );
          if (check.length === 0) {
            updatingItem.userProgress.push({
              ayah_number: item.ayah_number,
            });
            updatingItem.user_ayat = updatingItem.user_ayat + 1;
            resolve();
          }
        });
      })
      .catch(error => reject(error));
  });

export const getItemById = item =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then(realm => {
        let allItems = realm.objectForPrimaryKey(QURAN_SCHEMA, item.id);
        resolve(allItems);
      })
      .catch(error => {
        reject(error);
      });
  });

export default new Realm(databaseOptions);
