import getService from '../axios/getService';
import * as actionTypes from './actionTypes';

const fetchHokm = () => {
  return async (dispatch, getState) => {
    dispatch({type: actionTypes.FETCH_RINGS_TRIGGER, loading: true});
    return await getService('user/get-rings-times')
      .then(response => {
        // console.log(response);
        dispatch({
          type: actionTypes.FETCH_RINGS_SUCCESS,
          payload: response,
          loading: false,
        });
      })
      .catch(error => {
        dispatch({
          type: actionTypes.FETCH_RINGS_FAIL,
          payload: error.message,
          loading: false,
        });
      });
  };
};

export default fetchHokm;
