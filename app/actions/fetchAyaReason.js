import postService from '../axios/postService';
import * as actionTypes from './actionTypes';

const shuffleArray = array => {
  let i = array.length - 1;
  for (; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    const temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
  return array;
};

const fetchAyaReason = requestData => {
  return async (dispatch, getState) => {
    if (!requestData) {
      return Promise.reject(
        new Error("Please, make sure you've provided request data"),
      );
    }
    dispatch({type: actionTypes.GET_AYA_REASONS_TRIGGER, loading: true});
    return await postService('user/get-ayat-reasons', requestData)
      .then(async response => {
        // console.log(JSON.stringify(response.data, null, 2));()
        const ayaReasons = await shuffleArray(response.data);
        dispatch({
          type: actionTypes.GET_AYA_REASONS_SUCESS,
          payload: ayaReasons,
          loading: false,
        });
      })
      .catch(error => {
        dispatch({
          type: actionTypes.GET_AYA_REASONS_FAIL,
          payload: error.message,
          loading: false,
        });
      });
  };
};

export default fetchAyaReason;
