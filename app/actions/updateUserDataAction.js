import * as actionTypes from './actionTypes';
import {getStorage} from '../services/localStorageManager';

const fetchUser = () => {
  return async (dispatch, getState) => {
    const user = {...(await getStorage('data'))}.user;

    dispatch({
      type: actionTypes.FETCH_USER,
      user: user,
    });
  };
};

export default fetchUser;
