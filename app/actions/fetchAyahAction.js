import postService from '../axios/postService';
import * as actionTypes from './actionTypes';

const fetchAyah = requestData => {
  console.log('hshs: ', requestData);
  return async (dispatch, getState) => {
    if (!requestData) {
      return Promise.reject(
        new Error("Please, make sure you've provided request data"),
      );
    }
    dispatch({type: actionTypes.FETCH_AYAH_TRIGGER, loading: true});
    return await postService('/sora/ayah/audiowords', requestData)
      .then(response => {
        if (response.data.errorMessage === 'list index out of range') {
          // eslint-disable-next-line no-alert
          alert('this record not found');
          dispatch({
            type: actionTypes.RECORD_NOT_FOUND,
            payload: response.data,
            loading: false,
          });
        } else {
          dispatch({
            type: actionTypes.FETCH_AYAH_SUCCESS,
            payload: response.data,
            loading: false,
          });
        }
      })
      .catch(error => {
        dispatch({
          type: actionTypes.FETCH_AYAH_FAIL,
          payload: error.message,
          loading: false,
        });
      });
  };
};

export default fetchAyah;
