import postService from '../axios/postService';
import * as actionTypes from './actionTypes';

const fetchAyaReason = requestData => {
  return async (dispatch, getState) => {
    if (!requestData) {
      return Promise.reject(
        new Error("Please, make sure you've provided request data"),
      );
    }
    dispatch({type: actionTypes.GET_RECIT_QUIZ_TRIGGER, loading: true});
    return await postService('user/get-recite', requestData)
      .then(response => {
        // console.log(JSON.stringify(response.data, null, 2));
        dispatch({
          type: actionTypes.GET_RECIT_QUIZ_SUCESS,
          payload: response.data,
          loading: false,
        });
      })
      .catch(error => {
        dispatch({
          type: actionTypes.GET_RECIT_QUIZ_FAIL,
          payload: error.message,
          loading: false,
        });
      });
  };
};

export default fetchAyaReason;
