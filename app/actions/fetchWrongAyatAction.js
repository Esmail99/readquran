import postService from '../axios/postService';
import * as actionTypes from './actionTypes';
import {getStorage} from '../services/localStorageManager';

const fetchWrongAyatAction = () => {
  return async dispatch => {
    const data = await getStorage('data');
    const requestData = {
      user_id: data.user.id,
    };

    dispatch({type: actionTypes.FETCH_WRONG_AYAT_PENDING, loading: true});

    return await postService('user/get-wrong-ayat', requestData)
      .then(res => {
        dispatch({
          type: actionTypes.FETCH_WRONG_AYAT_SUCCESS,
          payload: res.data,
          loading: false,
        });
      })
      .catch(err => {
        dispatch({
          type: actionTypes.FETCH_WRONG_AYAT_FAIL,
          payload: err.message,
          loading: false,
        });
      });
  };
};

export default fetchWrongAyatAction;
