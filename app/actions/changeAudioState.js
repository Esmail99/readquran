import * as actionTypes from './actionTypes';

const changeAudioState = event => {
  return async dispatch => {
    dispatch({type: actionTypes.CHANGE_AUDIO_STATE, payload: event});
  };
};

export default changeAudioState;
