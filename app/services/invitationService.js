import postService from '../axios/postService';
import {getStorage} from './localStorageManager';
import axios from 'axios';

export const generateCode = async () => {
  const user = {...(await getStorage('data')).user};
  const data = {
    user_id: user.id,
  };
  const res = await (await postService('invitation/generate-code', data)).data;
  // console.log(res);
  return res;
};

export const decodeCode = async data => {
  // console.log(data);
  return await axios.post(
    'https://7astc125f5.execute-api.us-east-1.amazonaws.com/dev/invitation/decode-invitation-code',
    data,
  );
};
