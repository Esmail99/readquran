import React, {Component} from 'react';
import {Input, View, Item} from 'native-base';
import {StyleSheet} from 'react-native';

class SearchInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      onFocus: '',
    };
  }

  onchangeValue = async value => {
    const val = value.replace(/[- #*;,.<>\{\}\[\]\\\/]/gi, '');
    if (val.length !== 0) {
      this.props.getValue(val);
    }
  };

  onBlur = () => {
    this.setState({onFocus: false});
  };

  render() {
    return (
      <View style={styles.itemView}>
        <Item
          style={[
            styles.item,
            // eslint-disable-next-line react-native/no-inline-styles
            {borderColor: this.state.onFocus ? '#BFA115' : '#E5E4E4'},
          ]}
          rounded>
          <Input
            allowFontScaling={false}
            onFocus={() => {
              this.setState({onFocus: true});
            }}
            onBlur={() => {
              this.onBlur();
            }}
            placeholder={this.props.placeholder}
            style={styles.input}
            autoCapitalize="none"
            autoCorrect={false}
            autoCompleteType={'off'}
            onChangeText={value => {
              this.onchangeValue(value);
            }}
            value={this.state.value}
            placeholderTextColor="#C4C1C1"
            keyboardType={'number-pad'}
          />
        </Item>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  item: {
    alignSelf: 'center',
    width: 280,
    height: 50,
    borderRadius: 11,
    borderWidth: 1,
    borderBottomWidth: 3,
  },
  itemView: {
    marginBottom: 15,
    alignSelf: 'center',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    marginVertical: 15,
    marginHorizontal: 5,
  },
  input: {
    color: '#51565F',
    fontSize: 18,
    fontFamily: 'CairoRegular',
    textAlign: 'center',
    lineHeight: 33,
    alignSelf: 'center',
  },
});

export default SearchInput;
