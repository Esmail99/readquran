import React, {Component} from 'react';
import {StyleSheet, Image} from 'react-native';
import {Button, Footer, FooterTab} from 'native-base';

class FullAyaFooter extends Component {
  render() {
    const {onDownloadPress, onBookmarkPress, onSharePress} = this.props;

    return (
      <Footer style={styles.Footer}>
        <FooterTab style={styles.Footer}>
          <Button onPress={onDownloadPress}>
            <Image
              style={[styles.Image, styles.disabled]}
              source={require('../../assets/img/icon/download.png')}
            />
          </Button>
          <Button onPress={onBookmarkPress}>
            <Image
              style={styles.Image}
              source={require('../../assets/img/icon/bookmark.png')}
            />
          </Button>
          <Button onPress={onSharePress}>
            <Image
              style={[styles.Image, styles.disabled]}
              source={require('../../assets/img/icon/share.png')}
            />
          </Button>
        </FooterTab>
      </Footer>
    );
  }
}
const styles = StyleSheet.create({
  Footer: {
    backgroundColor: 'transparent',
    borderRadius: 2,
    borderBottomWidth: 0,
    borderColor: '#00000029',
    shadowColor: '#00000029',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.25,
    shadowRadius: 2,
    elevation: 1,
    width: '101%',
  },
  Image: {
    width: 25,
    height: 25,
  },
  disabled: {
    opacity: 0.4,
  },
});

export default FullAyaFooter;
