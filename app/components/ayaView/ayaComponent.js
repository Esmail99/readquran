/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text} from 'native-base';
import {StyleSheet, I18nManager} from 'react-native';
import connect from '../../connectedComponent/index';
import ReversedFlatList from 'react-native-reversed-flat-list';
import {FlatList} from 'react-native-gesture-handler';

class AyaComponent extends Component {
  constructor(props) {
    super(props);
  }

  renderListItem = ({item}) => {
    return (
      <Text
        allowFontScaling={false}
        style={[
          styles.arabicText,
          {
            borderBottomColor:
              item.word_index === this.props.index ? '#707070' : 'transparent',
          },
        ]}>
        {item?.arabic_word}
      </Text>
    );
  };

  getWords = () => {
    let words = [];
    if (this.props.index <= 6) {
      words = this.props.selectedAyaWords.slice(0, 7);
    } else if (
      this.props.index > 6 &&
      this.props.index % 7 < 7 &&
      this.props.index % 7 !== 0
    ) {
      words = this.props.selectedAyaWords.slice(
        this.props.index - (7 - (this.props.index % 7)),
        this.props.index + (this.props.index % 7),
      );
    } else {
      words = this.props.selectedAyaWords.slice(
        this.props.index - 3,
        this.props.index + 3,
      );
    }
    return words;
  };

  render() {
    return I18nManager.isRTL ? (
      <FlatList
        horizontal
        data={this.getWords()}
        contentContainerStyle={styles.list}
        renderItem={this.renderListItem}
        keyExtractor={(item, index) => index.toString()}
      />
    ) : (
      <ReversedFlatList
        horizontal
        data={this.getWords()}
        contentContainerStyle={styles.list}
        renderItem={this.renderListItem}
        keyExtractor={(item, index) => index.toString()}
      />
    );
  }
}

const styles = StyleSheet.create({
  arabicText: {
    fontSize: 18,
    lineHeight: 30,
    color: '#707070',
    fontFamily: 'Amiri',
    alignSelf: 'center',
    marginLeft: 2,
    marginRight: 2,
    borderBottomWidth: 1,
  },
  active: {
    fontSize: 25,
    color: '#54A889',
    fontFamily: 'Amiri',
    alignSelf: 'center',
    lineHeight: 50,
  },
});

export default connect(AyaComponent);
