/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  StyleSheet,
  TouchableHighlight,
  TouchableOpacity,
  Image,
} from 'react-native';
import Axios from 'axios';
import Sound from 'react-native-sound';
import PuaseIcon from 'react-native-vector-icons/AntDesign';
import connect from '../../connectedComponent/index';
import {Wave} from 'react-native-animated-spinkit';

class audioButton extends Component {
  sound = null;

  state = {
    loaded: false,
    paused: true,
    loading: false,
  };

  componentDidMount = () => {
    this.props.changeAudioState(false); //just to make sure whenever the component renders the audio button has initial state not activated.
  };

  getAudioUrl = async () => {
    const {wordUrl, sora, ayah} = this.props;

    let audioUrl = '';
    if (wordUrl) {
      audioUrl = wordUrl;
    } else {
      const KeyData = `${sora}:${ayah}`;
      const ayaNumber = (await Axios.get(
        `http://api.alquran.cloud/v1/ayah/${KeyData}`,
      )).data.data.number;
      audioUrl = `https://cdn.alquran.cloud/media/audio/ayah/ar.alafasy/${ayaNumber}`;
      // audioUrl = audioUrl;
    }
    return audioUrl;
  };

  load = async () => {
    const {audioButtonPlaying, changeAudioState} = this.props;

    if (!audioButtonPlaying) {
      await changeAudioState(true);
      return new Promise(async (resolve, reject) => {
        const audioUrl = await this.getAudioUrl();
        if (!audioUrl) {
          return reject('file path is empty');
        }

        this.sound = new Sound(audioUrl, null, error => {
          if (error) {
            console.log('failed to load the file', error);
            return reject(error);
          }
          this.setState({loaded: true});
          changeAudioState(false);
          return resolve();
        });
      });
    }
  };

  play = async () => {
    this.setState({loading: true});
    if (!this.state.loaded) {
      try {
        await this.load();
      } catch (error) {
        console.log(error);
      }
    }

    this.setState({paused: false, loading: false});
    Sound.setCategory('Playback');

    this.sound.play(success => {
      if (success) {
        console.log('successfully finished playing');
      } else {
        console.log('playback failed due to audio decoding errors');
      }
      this.setState({paused: true, loading: false});
      // this.sound.release();
    });
  };

  pause = () => {
    this.sound.pause();
    this.setState({paused: true});
  };

  playSound = async () => {
    if (this.pressed) {
      this.pressed = false;
      await this.pause();
    } else {
      this.pressed = true;
      await this.play();
    }
  };

  componentWillUnmount = () => {
    if (this.state.loaded) {
      this.sound.stop();
    }
  };

  render() {
    const {audioButtonPlaying} = this.props;
    return (
      <TouchableHighlight style={styles.highlight}>
        <TouchableOpacity
          disabled={audioButtonPlaying}
          onPress={async () => {
            requestAnimationFrame(() => {
              this.playSound();
            });
          }}>
          <Wave
            size={18}
            color="white"
            style={[
              styles.icon,
              {display: this.state.loading ? 'flex' : 'none'},
            ]}
          />
          {!this.state.loading ? (
            <>
              <Image
                source={require('../../assets/img/audio.png')}
                style={[
                  styles.image,
                  {display: !this.state.paused ? 'none' : 'flex'},
                ]}
              />
              <PuaseIcon
                name={'pause'}
                size={25}
                color={'white'}
                style={[
                  styles.icon,
                  {display: this.state.paused ? 'none' : 'flex'},
                ]}
              />
            </>
          ) : null}
        </TouchableOpacity>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  highlight: {
    borderRadius: 44 / 2,
    justifyContent: 'center',
    width: 44,
    height: 44,
    alignSelf: 'center',
    backgroundColor: '#E59D5C',
    borderColor: '#B77E4A',
    borderWidth: 1,
    borderBottomWidth: 3,
    marginHorizontal: 5,
  },
  icon: {
    alignSelf: 'center',
  },
  image: {
    width: 20,
    height: 20,
    alignSelf: 'center',
  },
});

export default connect(audioButton);
