import React, {Component} from 'react';
import {StyleSheet, Text, Image} from 'react-native';
import {View, Container} from 'native-base';
import connect from '../../connectedComponent/index';

class Loading extends Component {
  render() {
    const translate = this.props.language.text;

    return (
      <Container>
        <View style={styles.container}>
          <Image
            source={require('../../assets/img/logo.png')}
            style={styles.image}
          />
          <Text style={styles.loadingText}>{translate.Loading}</Text>
          <Text style={styles.text}>
            هل تعلم ان القران يحتوي علي ثمانية و عشرون سوره مدنيه و ستة وثمانون
            سورة مكيه
          </Text>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 30,
  },
  image: {
    height: 200,
    resizeMode: 'contain',
  },
  loadingText: {
    fontFamily: 'Amiri',
    fontSize: 24,
    color: '#aaaaaa',
  },
  text: {
    fontFamily: 'CairoRegular',
    fontSize: 15,
    color: '#707070',
    textAlign: 'center',
  },
});

export default connect(Loading);
