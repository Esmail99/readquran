import React, {Component} from 'react';
import {StyleSheet, ImageBackground, StatusBar} from 'react-native';
import {Container} from 'native-base';

class Layout extends Component {
  componentDidMount = () => {
    if (this.props.hideStatusBar) {
      StatusBar.setHidden(true, true);
    } else {
      StatusBar.setHidden(false, false);
    }
  };

  render() {
    return (
      <Container>
        <ImageBackground
          source={require('../../assets/img/background.png')}
          style={styles.backgroundImage}>
          {this.props.children}
          <StatusBar
            barStyle="light-content"
            hidden={false}
            backgroundColor={'#707070'}
          />
        </ImageBackground>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    width: '100%',
    height: '100%',
  },
});

export default Layout;
