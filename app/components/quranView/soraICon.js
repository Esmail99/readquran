import React, {Component} from 'react';
import {StyleSheet, Image, I18nManager} from 'react-native';
import {View} from 'native-base';

class SoraIcon extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {soraPlace} = this.props;
    return (
      <View style={I18nManager.isRTL ? styles.arabicImage : styles.image}>
        <Image
          source={require('../../assets/img/soraIcon.png')}
          style={styles.soraIcon}
        />
        {soraPlace === 'Makkah' ? (
          <Image
            source={require('../../assets/img/Makkah.png')}
            style={styles.soraType}
          />
        ) : (
          <Image
            source={require('../../assets/img/Madinah.png')}
            style={styles.soraType}
          />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    width: 33,
    height: 33,
    alignSelf: 'center',
  },
  arabicImage: {
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    width: 33,
    height: 33,
    alignSelf: 'center',
    position: 'absolute',
    left: 25,
  },
  soraIcon: {
    width: 32,
    height: 32,
  },
  soraType: {
    width: 14,
    height: 14,
    position: 'absolute',
  },
});

export default SoraIcon;
