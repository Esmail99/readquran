/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Actions} from 'react-native-router-flux';
import {StyleSheet, TouchableOpacity, I18nManager} from 'react-native';
import {View} from 'native-base';
import * as Progress from 'react-native-progress';
import SoraIcon from './soraICon';
import SoraDetails from './soraDetails';

class QuranList extends Component {
  constructor(props) {
    super(props);
  }

  selectReadingMood = () => {
    requestAnimationFrame(() => {
      Actions.readingMood({
        selectedItem: this.props.sora,
      });
    });
  };

  render() {
    const {sora} = this.props;

    return I18nManager.isRTL ? (
      <TouchableOpacity
        style={[
          styles.TouchableOpacity,
          {borderBottomWidth: sora.id === 114 ? 0 : 1},
        ]}
        disabled={sora.id === 114}
        onPress={() => this.selectReadingMood()}>
        <SoraDetails sora={sora} />
        <View style={styles.progressBar}>
          <Progress.Bar
            progress={sora.user_ayat / sora.number_of_ayat}
            width={260}
            height={10}
            color="#EFC91A"
            borderColor="transparent"
            unfilledColor="#ECEAEA"
          />
        </View>
      </TouchableOpacity>
    ) : (
      <TouchableOpacity
        style={[
          styles.TouchableOpacity,
          {borderBottomWidth: sora.id === 114 ? 0 : 1},
        ]}
        disabled={sora.id === 114}
        onPress={() => this.selectReadingMood()}>
        <SoraIcon soraPlace={sora.place} />
        <SoraDetails sora={sora} />
        <View style={styles.progressBar}>
          <Progress.Bar
            progress={sora.user_ayat / sora.number_of_ayat}
            width={260}
            height={10}
            color="#EFC91A"
            borderColor="transparent"
            unfilledColor="#ECEAEA"
          />
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  TouchableOpacity: {
    flexDirection: 'row',
    height: 100,
    justifyContent: 'space-between',
    alignContent: 'center',
    width: '94%',
    alignSelf: 'center',
    paddingBottom: 10,
    borderBottomColor: '#DBDBDB',
  },
  progressBar: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default QuranList;
