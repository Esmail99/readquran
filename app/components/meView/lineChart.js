import React from 'react';
import {StyleSheet, View, processColor, Text} from 'react-native';
import {LineChart} from 'react-native-charts-wrapper';
import moment from 'moment';
import postByUserId from '../../axios/postByUserId';
import {Actions} from 'react-native-router-flux';

class LineChartScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      nOfWrongAyat: [],
    };
  }

  componentDidMount = async () => {
    await postByUserId('user/get-chart-data')
      .then(res => {
        this.setState({nOfWrongAyat: res.data.result});
      })
      .catch(error => {
        console.log(error);
        if (error.message === 'Network Error') {
          Actions.noInternet();
        } else if (error.message.includes('400')) {
          this.setState({nOfWrongAyat: [0, 0, 0, 0, 0, 0, 0]});
        } else {
          Actions.noInternet({error: true});
        }
      });
  };

  getCurrentdate = () => {
    const weekEnd = moment();

    const days = [];
    for (let i = 0; i <= 6; i++) {
      days.push(
        moment(weekEnd)
          .subtract(i, 'days')
          .format('dd'),
      );
    }
    return days.reverse();
  };

  handleSelect(event) {
    let entry = event.nativeEvent;
    if (entry == null) {
      this.setState({...this.state, selectedEntry: null});
    } else {
      this.setState({...this.state, selectedEntry: JSON.stringify(entry)});
    }
  }

  render() {
    const week = this.getCurrentdate();
    const {withBackground} = this.props;

    return (
      <View
        style={[
          styles.container,
          // eslint-disable-next-line react-native/no-inline-styles
          {backgroundColor: !withBackground ? 'transparent' : '#fbfaf8'},
        ]}>
        <Text style={styles.NumberOfWrongAyat}>
          Number of <Text style={styles.bold}>wrong</Text> ayat
        </Text>
        <LineChart
          style={styles.chart}
          data={{
            dataSets: [
              {
                values: this.state.nOfWrongAyat,
                label: '',
                config: {
                  drawValues: false,
                  lineWidth: 2,
                  drawCircles: true,
                  circleColor: processColor('#e05859'),
                  drawCircleHole: true,
                  circleRadius: 5,
                  highlightColor: processColor('transparent'),
                  color: processColor('#e05859'),
                  drawFilled: false,
                  fillAlpha: 1000,
                  valueTextSize: 15,
                },
              },
            ],
          }}
          chartDescription={{
            text: `Today ${this.state.nOfWrongAyat[6] || 0} Ayat`,
          }}
          legend={{
            enabled: false,
          }}
          xAxis={{
            enabled: true,
            granularity: 1,
            drawLabels: true,
            position: 'BOTTOM',
            drawAxisLine: true,
            drawGridLines: false,
            fontFamily: 'CairoRegular',
            textSize: 12,
            textColor: processColor('#444'),
            valueFormatter: week,
          }}
          yAxis={{
            left: {
              enabled: true,
              granularity: 1,
              drawGridLines: false,
              textSize: 12,
              textColor: processColor('#444'),
            },
            right: {
              enabled: false,
            },
          }}
          autoScaleMinMaxEnabled={true}
          drawGridBackground={false}
          drawBorders={false}
          touchEnabled={false}
          dragEnabled={false}
          scaleEnabled={false}
          scaleXEnabled={false}
          scaleYEnabled={false}
          pinchZoom={false}
          doubleTapToZoomEnabled={false}
          dragDecelerationEnabled={true}
          dragDecelerationFrictionCoef={0.99}
          keepPositionOnRotation={false}
          onSelect={this.handleSelect.bind(this)}
          onChange={event => console.log(event.nativeEvent)}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fbfaf8',
    marginVertical: 15,
    paddingBottom: 15,
  },
  chart: {
    height: 125,
  },
  NumberOfWrongAyat: {
    fontSize: 9,
    color: '#707070',
    marginTop: 5,
    marginLeft: 5,
    marginBottom: -15,
  },
  bold: {
    fontWeight: 'bold',
  },
});

export default LineChartScreen;
