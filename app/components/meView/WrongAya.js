import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
} from 'react-native';
import {Text} from 'native-base';
import Sound from 'react-native-sound';
import RNFS from 'react-native-fs';
import Share from 'react-native-share';
import ReversedFlatList from 'react-native-reversed-flat-list';

class WrongAya extends Component {
  constructor() {
    super();
    this.state = {
      englishAya: '',
    };
  }

  componentDidMount = () => {
    const {aya} = this.props;

    const englishAya = aya.words_with_ahkam
      .map(word => {
        return word.english_word;
      })
      .toString()
      .replace(/,/g, ' ');

    this.setState({englishAya});
  };

  playSound = async audioUrl => {
    this.setState({playing: true});
    return new Promise(async resolve => {
      const track = new Sound(audioUrl, null, e => {
        if (e) {
          console.log('error loading track:', e);
          this.setState({playing: false});
        } else {
          this.setState({playing: false});
          track.play(resolve);
        }
      });
    });
  };

  share = async () => {
    const {user_record_base64} = this.props.aya;
    const path = `${RNFS.DocumentDirectoryPath}/audio.mp3`;
    RNFS.writeFile(path, user_record_base64, 'base64')
      .then(res => {
        // this.playSound(path);
        Share.open({
          url: 'file://' + path,
          type: 'audio/mp3',
        });
      })
      .catch(err => console.onPress(err));
  };

  renderItem = ({item}) => {
    const {user_record_base64} = this.props.aya;

    return (
      <TouchableOpacity
        style={styles.word}
        onPress={() => {
          this.playSound(user_record_base64);
        }}>
        <Text
          allowFontScaling={false}
          style={[
            styles.arabicTextWrong,
            item.status === 'true' ? styles.arabicTextCorrect : null,
          ]}>
          {item.arabic_word}
        </Text>
        <Text
          allowFontScaling={false}
          style={[
            styles.frankoTextWrong,
            item.status === 'true' ? styles.frankoTextCorrect : null,
          ]}>
          {item.english_slang}
        </Text>
      </TouchableOpacity>
    );
  };

  render() {
    const {englishAya} = this.state;

    return (
      <>
        <TouchableOpacity style={styles.container}>
          <View style={styles.ayaView}>
            <ImageBackground
              source={require('../../assets/img/soraIcon.png')}
              style={styles.ayaImage}>
              <Text allowFontScaling={false} style={styles.ayaText}>
                {this.props.aya.id}
              </Text>
            </ImageBackground>
            <ReversedFlatList
              data={this.props.aya.words_with_ahkam}
              contentContainerStyle={styles.reversedList}
              renderItem={this.renderItem}
              numColumns={5}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
          <Text allowFontScaling={false} style={styles.englishText}>
            {englishAya}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.shareBtn} onPress={this.share}>
          <Image
            source={require('../../assets/img/icon/share.png')}
            style={styles.shareImage}
          />
        </TouchableOpacity>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#DBDBDB',
    width: '95%',
    alignSelf: 'center',
  },
  shareBtn: {
    width: 50,
    height: 60,
    position: 'absolute',
    left: 0,
    top: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  shareImage: {
    width: 20,
    height: 20,
  },
  ayaView: {
    flexDirection: 'row-reverse',
    alignSelf: 'flex-end',
  },
  ayaImage: {
    width: 26,
    height: 26,
    alignSelf: 'center',
    position: 'absolute',
    top: 20,
  },
  ayaText: {
    position: 'absolute',
    alignSelf: 'center',
    fontSize: 7,
    color: '#707070',
    top: 5,
    lineHeight: 15,
    fontFamily: 'CairoBold',
  },
  reversedList: {
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    marginVertical: 15,
  },
  word: {
    marginHorizontal: 4,
  },
  arabicTextWrong: {
    fontSize: 20,
    color: '#D64944',
    lineHeight: 53,
    fontFamily: 'Amiri',
    alignSelf: 'center',
  },
  arabicTextCorrect: {
    color: '#59AC4F',
  },
  frankoTextWrong: {
    fontSize: 17,
    fontFamily: 'CairoRegular',
    color: '#D64944',
    alignSelf: 'center',
    lineHeight: 30,
  },
  frankoTextCorrect: {
    color: '#59AC4F',
  },
  englishText: {
    fontSize: 10,
    fontFamily: 'CairoRegular',
    color: '#707070',
    lineHeight: 15,
    alignSelf: 'center',
  },
});

export default WrongAya;
