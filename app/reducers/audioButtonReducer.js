import * as actionTypes from '../actions/actionTypes.js';

const initialState = {
  playing: false,
};

const audioButtonReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CHANGE_AUDIO_STATE:
      return {...state, playing: action.payload};
    default:
      return state;
  }
};

export default audioButtonReducer;
