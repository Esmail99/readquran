import * as actionTypes from '../actions/actionTypes.js';

const initialState = {
  imageUrl: null,
};

const fetchUserImageReducer = (state = initialState, action) => {
  if (action === undefined) {
    return state;
  }
  switch (action.type) {
    case actionTypes.FETCH_IMAGE:
      return {
        imageUrl: action.url,
      };
    default:
      return state;
  }
};
export default fetchUserImageReducer;
