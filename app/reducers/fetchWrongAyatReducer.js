import * as actionTypes from '../actions/actionTypes.js';

const initialState = {
  ayat: [],
  loading: false,
  errorMessage: null,
};

const fetchWrongAyatReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_WRONG_AYAT_PENDING:
      return {...state, loading: action.loading};
    case actionTypes.FETCH_WRONG_AYAT_SUCCESS:
      return {
        ...state,
        ayat: action.payload,
        loading: action.loading,
      };
    case actionTypes.FETCH_WRONG_AYAT_FAIL:
      return {...state, errorMessage: action.payload, loading: action.loading};
    default:
      return state;
  }
};

export default fetchWrongAyatReducer;
