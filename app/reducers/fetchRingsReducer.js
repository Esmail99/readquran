import * as actionTypes from '../actions/actionTypes.js';

const initialState = {
  rings: [],
  loading: false,
  errorMessage: null,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    // focus action is dispatched when a new screen comes into focus
    case actionTypes.FETCH_RINGS_TRIGGER:
      return {...state, loading: action.loading};
    case actionTypes.FETCH_RINGS_SUCCESS:
      return {
        ...state,
        rings: action.payload,
        loading: action.loading,
      };
    case actionTypes.FETCH_RINGS_FAIL:
      return {...state, errorMessage: action.payload, loading: action.loading};
    default: {
      return {
        ...state,
      };
    }
  }
}
