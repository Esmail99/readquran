import * as actionTypes from '../actions/actionTypes.js';
import {NativeModules} from 'react-native';

const setLanguage = language => {
  let languages = {};
  switch (language) {
    case 'ar':
      languages = Object.assign(languages, require('../translations/ar.json'));
      break;
    case 'gr':
      languages = Object.assign(languages, require('../translations/gr.json'));
      break;
    case 'fr':
      languages = Object.assign(languages, require('../translations/fr.json'));
      break;
    case 'sp':
      languages = Object.assign(languages, require('../translations/sp.json'));
      break;
    default:
    case 'en':
      languages = Object.assign(languages, require('../translations/en.json'));
      break;
  }
  return languages;
};

const checkRtl = language => {
  return language === 'ar' ? true : false;
};

const initialState = {
  locale: NativeModules.I18nManager.localeIdentifier.substring(0, 2),
  text: setLanguage(NativeModules.I18nManager.localeIdentifier.substring(0, 2)),
  rtl: checkRtl(NativeModules.I18nManager.localeIdentifier.substring(0, 2)),
};

const intlLanguage = (state = initialState, action) => {
  if (action === undefined) {
    return state;
  }
  switch (action.type) {
    case actionTypes.UPDATE_LANGUAGE:
      return {
        locale: action.language,
        text: setLanguage(action.language),
        rtl: checkRtl(action.language),
      };
    default:
      return state;
  }
};
export default intlLanguage;
